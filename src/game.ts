const carcas = new Entity();
const floor = new Entity();
const carcasTransform = new Transform();
const floorTransform = new Transform();

carcasTransform.position = new Vector3(16,1,8);
carcasTransform.scale = new Vector3(1,1,1);
floorTransform.position = new Vector3(0,0,0);
floorTransform.scale = new Vector3(1,1,1);


carcas.addComponent(new GLTFShape("models/carcas.glb"));
carcas.addComponent(carcasTransform);
floor.addComponent(new GLTFShape("models/floor.glb"));
floor.addComponent(floorTransform);

engine.addEntity(carcas);
engine.addEntity(floor);